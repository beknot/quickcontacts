package com.example.asterisk.maps;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

public class DisplayNear extends AppCompatActivity {                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           String[] names = {"Red Cross Balkot Sub-Branch","Kathmandu Medical College","Civil Service Hospital of Nepal","Kist Medical College Teaching Hospital","Tilganga Eye Hospital","Patan hospital","Kathmandu Clinic Of Cosmetic Surgery","Norvic International Hospital","Medicare National Hospital & Research Center","Kathmandu Model Hospital","Om Hospital & Research Center","Bir Hospital","Star Hospital","Shukraraaj Tropical & Infectious Disease Hospital","Bhaktapur Cancer Hospital","CIWEC Hospital Pvt. Ltd.","Nepal Dental Hospital","T.U. Teaching Hospital"};String phone[] = {"","014469064","014107000","015201496","014493684","015522295","014771045","014258554","014467067","014222450","014476225","014221119","015550197","014253396","016611532","014424111","014433697","014414404"}; String latt[] = {"27.669601","27.695917","27.686336","27.662998","27.705669","27.668376","27.699694","27.690048","27.717653","27.702622","27.721477","27.704920","27.681636","27.695450","27.673138","27.720466","27.729053","27.736078"}; String lngg[] = {"85.366041","85.353380","85.338812","85.335105","85.350454","85.320567","85.327949","85.319070","85.346071","85.320140","85.344804","85.313651","85.302507","85.306542","85.422221","85.317729","85.325359","85.330264"};Double lat[] = {27.669601,27.695917,27.686336,27.662998,27.705669,27.668376,27.699694,27.690048,27.717653,27.702622,27.721477,27.704920,27.681636,27.695450,27.673138,27.720466,27.729053,27.736078}; Double lng[] = {85.366041,85.353380,85.338812,85.335105,85.350454,85.320567,85.327949,85.319070,85.346071,85.320140,85.344804,85.313651,85.302507,85.306542,85.422221,85.317729,85.325359,85.330264};Double startLat=27.676099;Double startLng=85.361190;
//Lokanthali
    //String []names={"n","a","m","e"};
    ListView lv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.display_near);

        ListView displaylist  = findViewById(R.id.displaylist);
        displaylist.setAdapter(new MyAdapter2(DisplayNear.this,names,latt,lngg,startLat.toString(),startLng.toString()));


        final int EARTH_RADIUS = 6371;
       /* Double []lat={27.00};
        Double []lng={30.00};

        Double startLat=0.00;
        Double startLng=0.00;*/
        Double endLat;
        Double endLng;
        int n = lat.length;
        Double result[] = new Double[n];
        Double d=0.0000,dist=0.0000;
        Double distance=0.0000,shortest=0.0000;

        for(int i=0;i<n;i++) {
            endLat = lat[i];
            endLng = lng[i];
            double dLat = Math.toRadians((endLat - startLat));
            double dLong = Math.toRadians((endLng - startLng));

            double startLats = Math.toRadians(startLat);
            double endLats = Math.toRadians(endLat);

            d = haversin(dLat) + Math.cos(startLats) * Math.cos(endLats) * haversin(dLong);
            dist = 2 * Math.atan2(Math.sqrt(d), Math.sqrt(1 - d));

            distance = EARTH_RADIUS * dist; // <-- d

            result[i] = distance;

            System.out.println((i+1)+""+"(startLat,startLng) [D]: ("+startLat+","+startLng+")");
            System.out.println("(endLat,endLng) [D]: ("+endLat+","+endLng+")");
            System.out.println("startlat [R]: "+startLats+" endLat[R]: "+endLats);
            System.out.println("d: "+d+"  dist: "+dist);
            System.out.println("distance: "+distance);
            System.out.println("result: "+result[i]);
            System.out.println("______________________________");

        }
        for(int i=0;i<n;i++) {
            for(int j=i+1;j<n;j++) {
                if (result[i] >result[j]){
                    double c = result[i];
                    result[i] = result[j];
                    result[j] = c;

                }
            }
        }
        for(int i=0;i<n;i++) {
            System.out.println(result[i]+", ");
        }}

    public static double haversin(double val) {
        return Math.pow(Math.sin(val / 2), 2);
    }
}