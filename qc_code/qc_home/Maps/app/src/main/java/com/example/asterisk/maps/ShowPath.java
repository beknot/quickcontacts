package com.example.asterisk.maps;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.CancellationSignal;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ShowPath extends FragmentActivity implements OnMapReadyCallback {

    private static final int LOCATION_REQUEST = 500;

    private GoogleMap mMap;
    TextView all;
    String name,sLat,sLng,dLat,dLng;
    Double startLat,startLng,destLat,destLng;
    LatLng start,destination;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_path);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        all = findViewById(R.id.tv1);

        Bundle b = new Bundle();
        b = getIntent().getExtras();
        name = b.getString("name");
        sLat = b.getString("startLat");
        sLng = b.getString("startLng");
        dLat = b.getString("lat");
        dLng = b.getString("lng");

        all.setText("From Your position to "+name+"\n ("+sLat+","+sLng+") ("+dLat+","+dLng+")");

        start = new LatLng(Double.parseDouble(sLat),Double.parseDouble(sLng));
        destination = new LatLng(Double.parseDouble(dLat),Double.parseDouble(dLng));

        startLat = Double.parseDouble(sLat);
        startLng = Double.parseDouble(sLng);
        destLat = Double.parseDouble(dLat);
        destLng = Double.parseDouble(dLng);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if(name.equals("Korea-Nepal Friendship Hospital")) {

            LatLng begin = new LatLng(startLat,startLng);
            mMap.addMarker(new MarkerOptions().position(begin).title("Your location"));
            LatLng dest = new LatLng(destLat,destLng);
            mMap.addMarker(new MarkerOptions().position(dest).title(name));

            mMap.addPolyline(new PolylineOptions().add(begin,
                    //upto thimi
                    new LatLng(27.690642, 85.389604),new LatLng(27.690615, 85.389695),new LatLng(27.690594, 85.389738),
                    new LatLng(27.689933, 85.390437),new LatLng(27.689883, 85.390376),new LatLng(27.689831, 85.390279),
                    new LatLng(27.689712, 85.389989),new LatLng(27.689642, 85.389899),new LatLng(27.689376, 85.389668),
                    new LatLng(27.689227, 85.389566),new LatLng(27.689065, 85.389470),new LatLng(27.688755, 85.389342),
                    new LatLng(27.688652, 85.389282),new LatLng(27.688396, 85.389102),new LatLng(27.687599, 85.388526),
                    new LatLng(27.687235, 85.388326),new LatLng(27.686943, 85.388197),new LatLng(27.686302, 85.387994),
                    new LatLng(27.685759, 85.387898),new LatLng(27.685647, 85.387867),new LatLng(27.685492, 85.387801),
                    new LatLng(27.685295, 85.387681),new LatLng(27.685246, 85.387640),new LatLng(27.684987, 85.387615),
                    new LatLng(27.684334, 85.387631),new LatLng(27.683822, 85.387505),new LatLng(27.683682, 85.387480),
                    new LatLng(27.683484, 85.387435),new LatLng(27.683104, 85.387299),

                    new LatLng(27.683112, 85.387251),new LatLng(27.683101, 85.387111),new LatLng(27.682973, 85.386258),
                    new LatLng(27.682701, 85.384745), new LatLng(27.682463, 85.383231),new LatLng(27.681856, 85.383155),
                    new LatLng(27.681707, 85.383111),new LatLng(27.681681, 85.383095),new LatLng(27.681491, 85.382897),
                    new LatLng(27.681396, 85.382814),new LatLng(27.681343, 85.382784),new LatLng(27.681116, 85.382722),
                    new LatLng(27.680933, 85.382718),new LatLng(27.680781, 85.382701),new LatLng(27.680742, 85.382710),
                    new LatLng(27.680706, 85.382726),new LatLng(27.680668, 85.382761),new LatLng(27.680558, 85.382884),
                    new LatLng(27.680396, 85.383003),new LatLng(27.680268, 85.383046),new LatLng(27.679728, 85.383352),
                    new LatLng(27.679617, 85.383378),new LatLng(27.678793, 85.383393),new LatLng(27.678664, 85.383411),

                    dest).width(10).color(Color.GREEN));

            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(begin, 14));
        } else if(name.equals("Bhaktapur Cancer Hospital")) {LatLng begin = new LatLng(startLat,startLng);
            mMap.addMarker(new MarkerOptions().position(begin).title("Your location"));
            LatLng dest = new LatLng(destLat,destLng);
            mMap.addMarker(new MarkerOptions().position(dest).title(name));

            mMap.addPolyline(new PolylineOptions().add(begin,

                    new LatLng(27.690642, 85.389604),new LatLng(27.690615, 85.389695),new LatLng(27.690594, 85.389738),
                    new LatLng(27.689933, 85.390437),new LatLng(27.689883, 85.390376),new LatLng(27.689831, 85.390279),
                    new LatLng(27.689712, 85.389989),new LatLng(27.689642, 85.389899),new LatLng(27.689376, 85.389668),
                    new LatLng(27.689227, 85.389566),new LatLng(27.689065, 85.389470),new LatLng(27.688755, 85.389342),
                    new LatLng(27.688652, 85.389282),new LatLng(27.688396, 85.389102),new LatLng(27.687599, 85.388526),
                    new LatLng(27.687235, 85.388326),new LatLng(27.686943, 85.388197),new LatLng(27.686302, 85.387994),
                    new LatLng(27.685759, 85.387898),new LatLng(27.685647, 85.387867),new LatLng(27.685492, 85.387801),
                    new LatLng(27.685295, 85.387681),new LatLng(27.685246, 85.387640),new LatLng(27.684987, 85.387615),
                    new LatLng(27.684334, 85.387631),new LatLng(27.683822, 85.387505),new LatLng(27.683682, 85.387480),
                    new LatLng(27.683484, 85.387435),new LatLng(27.683104, 85.387299),new LatLng(27.682739, 85.389340),
                    new LatLng(27.682669, 85.389814),new LatLng(27.682601, 85.390067),new LatLng(27.681269, 85.392637),
                    new LatLng(27.681149, 85.392986),new LatLng(27.679463, 85.400930),new LatLng(27.679421, 85.401194),
                    new LatLng(27.678775, 85.404196),new LatLng(27.678128, 85.407030),new LatLng(27.677956, 85.407402),
                    new LatLng(27.677803, 85.407539),new LatLng(27.677186, 85.408013),
                    new LatLng(27.675156, 85.409268),new LatLng(27.674283, 85.409712),new LatLng(27.673969, 85.409976),
                    new LatLng(27.673742, 85.410390),new LatLng(27.672809, 85.412507),
                    new LatLng(27.672214, 85.413684),new LatLng(27.671941, 85.414119),new LatLng(27.671874, 85.414254),
                    new LatLng(27.671772, 85.414535),new LatLng(27.671734, 85.414691),new LatLng(27.671656, 85.415062),
                    new LatLng(27.671619, 85.415273),new LatLng(27.671430, 85.418149),new LatLng(27.671338, 85.419836),
                    new LatLng(27.671331, 85.420631),new LatLng(27.671343, 85.420998),new LatLng(27.671358, 85.421194),
                    new LatLng(27.671393, 85.421363),new LatLng(27.671556, 85.421543),new LatLng(27.671613, 85.421636),
                    new LatLng(27.671965, 85.422363),new LatLng(27.671998, 85.422407),new LatLng(27.672078, 85.422421),
                    new LatLng(27.672937, 85.422449),new LatLng(27.673064, 85.422475),new LatLng(27.673104, 85.422194),

                    dest).width(10).color(Color.GREEN));

            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(begin, 14));
        } else {
            Toast.makeText(ShowPath.this,"Fail to obtain path",Toast.LENGTH_SHORT).show();
        }


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
             return;
        }
        mMap.setMyLocationEnabled(true);

        String url = getRequestUrl(start, destination);
        ShowPath.TaskRequestDirections taskRequestDirections = new ShowPath.TaskRequestDirections();
        taskRequestDirections.execute(url);


    }

    private String getRequestUrl(LatLng origin, LatLng dest) {
        //Value of origin
        String str_org = "origin=" + origin.latitude +","+origin.longitude;
        //Value of destination
        String str_dest = "destination=" + dest.latitude+","+dest.longitude;
        //Set value enable the sensor
        String sensor = "sensor=false";
        //Mode for find direction
        String mode = "mode=driving";
        //Build the full param
        String param = str_org +"&" + str_dest + "&" +sensor+"&" +mode;
        //Output format
        String output = "json";
        //Create url to request
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + param;
        return url;
    }

    private String requestDirection(String reqUrl) throws IOException {
        String responseString = "";
        InputStream inputStream = null;
        HttpURLConnection httpURLConnection = null;
        try{
            URL url = new URL(reqUrl);
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.connect();

            //Get the response result
            inputStream = httpURLConnection.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

            StringBuffer stringBuffer = new StringBuffer();
            String line = "";
            while ((line = bufferedReader.readLine()) != null) {
                stringBuffer.append(line);
            }

            responseString = stringBuffer.toString();
            bufferedReader.close();
            inputStreamReader.close();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
            httpURLConnection.disconnect();
        }
        return responseString;
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case LOCATION_REQUEST:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mMap.setMyLocationEnabled(true);
                }
                break;
        }
    }

    public class TaskRequestDirections extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... strings) {
            String responseString = "";
            try {
                responseString = requestDirection(strings[0]);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return  responseString;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            //Parse json here
            ShowPath.TaskParser taskParser = new ShowPath.TaskParser();
            taskParser.execute(s);
        }
    }

    public class TaskParser extends AsyncTask<String, Void, List<List<HashMap<String, String>>> > {

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... strings) {
            JSONObject jsonObject = null;
            List<List<HashMap<String, String>>> routes = null;
            try {
                jsonObject = new JSONObject(strings[0]);
                DirectionsParser directionsParser = new DirectionsParser();
                routes = directionsParser.parse(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> lists) {
            //Get list route and display it into the map

            ArrayList points = null;

            PolylineOptions polylineOptions = null;

            for (List<HashMap<String, String>> path : lists) {
                points = new ArrayList();
                polylineOptions = new PolylineOptions();

                for (HashMap<String, String> point : path) {
                    double lat = Double.parseDouble(point.get("lat"));
                    double lon = Double.parseDouble(point.get("lon"));

                    points.add(new LatLng(lat,lon));
                }

                polylineOptions.addAll(points);
                polylineOptions.width(0);
                polylineOptions.color(Color.TRANSPARENT);
                polylineOptions.geodesic(true);
            }

            if (polylineOptions!=null) {
                mMap.addPolyline(polylineOptions);
            } else {
                Toast.makeText(getApplicationContext(), "Fetching direction!", Toast.LENGTH_SHORT).show();
            }

        }
    }
}
