package com.example.asterisk.maps;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;
//Bode
public class DisplayNear extends AppCompatActivity {                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         String[] names = {"Korea-Nepal Friendship Hospital","Red Cross Balkot Sub-Branch","Nepal Orthopedic Hospital", "Kathmandu Medical College","Bhaktapur Cancer Hospital","Tilganga Eye Hospital","Civil Service Hospital of Nepal","Medicare National Hospital & Research Center","Om Hospital & Research Center","Kathmandu Clinic Of Cosmetic Surgery","Kist Medical College Teaching Hospital","Norvic International Hospital","Kathmandu Model Hospital","Patan hospital","Nepal Dental Hospital","Bir Hospital","TU Teaching Hospital","CIWEC Hospital Pvt. Ltd."};String phone[] = {"016633442","","014911725","014469064","016611532","014493684","014107000","014467067","014476225","015201496","014771045","014258554","014222450","015522295","014433697","014221119","014414404","014424111"}; String latt[] = {"27.678701","27.669601","27.720213","27.695917","27.673138","27.705669","27.686336","27.717653","27.721477","27.699694","27.662998","27.690048","27.702622","27.668376","27.729053","27.704920","27.736078","27.720466"}; String lngg[] = {"85.383810","85.366041","85.381202","85.353380","85.422221","85.350454","85.338812","85.346071","85.344804","85.327949","85.335105","85.319070","85.320140","85.320567","85.325359","85.313651","85.330264","85.317729"}; Double lat[] = {27.678701,27.669601,27.720213,27.695917,27.673138,27.705669,27.686336,27.717653,27.721477,27.699694,27.662998,27.690048,27.702622,27.668376,27.729053,27.704920,27.736078,27.720466}; Double lng[] = {85.383810,85.366041,85.381202,85.353380,85.422221,85.350454,85.338812,85.346071,85.344804,85.327949,85.335105,85.319070,85.320140,85.320567,85.325359,85.313651,85.330264,85.317729};Double startLat=27.6907037;Double startLng=85.3895544;

    //String []names={"n","a","m","e"};
    ListView lv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.display_near);

        ListView displaylist  = findViewById(R.id.displaylist);
        displaylist.setAdapter(new MyAdapter2(DisplayNear.this,names,latt,lngg,startLat.toString(),startLng.toString()));


        final int EARTH_RADIUS = 6371;
       /* Double []lat={27.00};
        Double []lng={30.00};

        Double startLat=0.00;
        Double startLng=0.00;*/
        Double endLat;
        Double endLng;
        int n = lat.length;
        Double result[] = new Double[n];
        Double d=0.0000,dist=0.0000;
        Double distance=0.0000,shortest=0.0000;

        for(int i=0;i<n;i++) {
            endLat = lat[i];
            endLng = lng[i];
            double dLat = Math.toRadians((endLat - startLat));
            double dLong = Math.toRadians((endLng - startLng));

            double startLats = Math.toRadians(startLat);
            double endLats = Math.toRadians(endLat);

            d = haversin(dLat) + Math.cos(startLats) * Math.cos(endLats) * haversin(dLong);
            dist = 2 * Math.atan2(Math.sqrt(d), Math.sqrt(1 - d));

            distance = EARTH_RADIUS * dist; // <-- d

            result[i] = distance;

            System.out.println((i+1)+""+"(startLat,startLng) [D]: ("+startLat+","+startLng+")");
            System.out.println("(endLat,endLng) [D]: ("+endLat+","+endLng+")");
            System.out.println("startlat [R]: "+startLats+" endLat[R]: "+endLats);
            System.out.println("d: "+d+"  dist: "+dist);
            System.out.println("distance: "+distance);
            System.out.println("result: "+result[i]);
            System.out.println("______________________________");

        }
        for(int i=0;i<n;i++) {
            for(int j=i+1;j<n;j++) {
                if (result[i] >result[j]){
                    double c = result[i];
                    result[i] = result[j];
                    result[j] = c;

                }
            }
        }
        for(int i=0;i<n;i++) {
            System.out.println(result[i]+", ");
        }}

    public static double haversin(double val) {
        return Math.pow(Math.sin(val / 2), 2);
    }
}