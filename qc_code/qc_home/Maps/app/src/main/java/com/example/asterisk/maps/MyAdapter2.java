package com.example.asterisk.maps;


import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

class MyAdapter2 extends BaseAdapter {
    Context c;
    String[] myN,myLat,myLng;
    String sLat,sLng;
    public MyAdapter2(DisplayNear displayNear, String[] names,String[] latt,String[] lngg,String startLat,String startLng) {
        c = displayNear;
        myN = names;
        myLat = latt;
        myLng = lngg;
        sLat = startLat;
        sLng = startLng;
    }

    @Override
    public int getCount() {
        return myN.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(c).inflate(R.layout.nearlayout,null);
        TextView name=convertView.findViewById(R.id.tv1);
        // TextView number=convertView.findViewById(R.id.tv2);
        //TextView latitude=convertView.findViewById(R.id.tv3);
        //TextView longitude=convertView.findViewById(R.id.tv4);
        name.setText(String.valueOf(myN[position]));
        //number.setText(String.valueOf(myp[position]));
        //latitude.setText(String.valueOf(myLat[position]));
        //longitude.setText(String.valueOf(myLng[position]));

        final View finalConvertView = convertView;
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(c.getApplicationContext(),ShowPath.class);
                i.putExtra("name", myN[position]);
                i.putExtra("startLat", sLat);
                i.putExtra("startLng", sLng);
                i.putExtra("lat", myLat[position]);
                i.putExtra("lng", myLng[position]);
                c.startActivity(i);
            }
        });

        return convertView;
    }
}
