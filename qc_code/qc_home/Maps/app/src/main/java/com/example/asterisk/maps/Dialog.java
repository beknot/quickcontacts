package com.example.asterisk.maps;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import static android.content.Context.NOTIFICATION_SERVICE;

public class Dialog extends AppCompatDialogFragment {

    EditText et1,et2,et3,et4;
    DialogListener listener;
    String name,issue,bloodNno;
    @Override
    public android.app.Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog,null);

        et1 = view.findViewById(R.id.et1);
        et2 = view.findViewById(R.id.et2);
        et3 = view.findViewById(R.id.et3);
        et4 = view.findViewById(R.id.et4);
        builder.setView(view).setTitle("Notification").setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                name = "Name:  "+(et1.getText().toString());
                issue = "Health issue:  "+(et2.getText().toString());
                bloodNno= "Caretakers no:  "+((et3.getText().toString()))+"  Blood Group:  "+((et4.getText().toString()));
                listener.applyTexts(name,issue,bloodNno);
                Intent i = new Intent(getActivity(),HomeFragment.class);
                TaskStackBuilder stackBuilder = TaskStackBuilder.create(getActivity());
                stackBuilder.addParentStack(HomeFragment.class);
                stackBuilder.addNextIntent(i);
                PendingIntent intent = PendingIntent.getActivity(getActivity(),0,i,0);
                android.app.Notification notification = new android.app.Notification.Builder(getActivity())
                        .setTicker("Title").setContentTitle(name).setContentText(issue)
                        .setSubText(bloodNno).setSmallIcon(R.drawable.q).setPriority(android.app.Notification.PRIORITY_HIGH)
                        .setContentIntent(intent).getNotification();
                notification.flags =  android.app.Notification.FLAG_AUTO_CANCEL;
                NotificationManager manager = (NotificationManager) getActivity().getSystemService(NOTIFICATION_SERVICE);
                manager.notify(0,notification);
            }
        });
        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (DialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()+" must implement DialogListener");
        }
    }

    public interface DialogListener {
        void applyTexts(String name,String address,String bloodNno);
    }
}
